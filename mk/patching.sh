#!/bin/sh

WORK_DIR=$1
FILES_DIR=$2

ls $FILES_DIR/patch* 2>/dev/null
status=$?

if [ $status == 0 ] ; then
 
 PATCH_FILES=`ls $FILES_DIR/patch*`
 cd $WORK_DIR && for i in $PATCH_FILES; do patch -p0 < $i; done
 exit 0

else
 echo "No patches available"
 exit 0
fi

